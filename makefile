CC=gcc
CFLAGS= -W -Wall -Wextra
LDFLAGS=
EXEC= projet_PA
BINPATH=bin/
SRCPATH=source/
OBJ=obj/

all: $(EXEC)

$(EXEC): $(OBJ)fichier_complet.o
	mkdir -p $(BINPATH)
	$(CC) $(CFLAGS) -o $(BINPATH)$(EXEC) $(OBJ)fichier_complet.o $(LDFLAGS)

$(OBJ)fichier_complet.o: $(SRCPATH)fichier_complet.c
	mkdir -p $(OBJ)
	$(CC) $(CFLAGS) -o $(OBJ)fichier_complet.o -c $(SRCPATH)fichier_complet.c $(LDFLAGS)

clean:
	rm -rf $(BINPATH)