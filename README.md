# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépot `GIT` contient les livrables du binôme composé des étudiants: __Lacroix Héloïse__ et __Vain Arthur__ pour le projet de Programmation avancé du semestre 6.

## Résumé

L'objectif de ce projet est de vérifier que notre maîtrise sur les principes vus en
cours de programmation avancée : structures de données complexes,
lecture / écriture de fichiers, compilation séparée et automatique, utilisation
de gestionnaire de version...

Pour cela, le travail demandé est de réaliser une application qui permet d'analyser 58592 vols aux États Unis en 2014. Votre travail est de charger ces fichiers pour effectuer un certain nombre de requêtes (lister des vols selon plusieurs critères, lister l'aéroligne avec plus de retards, trouver un itinéraire, ...)


## Cahier des charges
Le principe de ce projet est de réaliser une application permettant d’analyser 58592 vols aux Etats Unis en 2014. Pour cela nous disposons de 3 fichiers CSV format texte, stockant différentes données :

Le premier fichier concerne les __vols__ (flights.csv) et contient les champs suivants :
	* Le `mois` du vol (int)
	* La date du `jour` du vol (int)
	* Le `jour de la semaine` (int)
	* La `compagnie aérienne` (char)
	* L’`aéroport de départ` (char)
	* L’`aéroport de destination` (char)
	* L’`heure de départ prévue` (int)
	* Le `délai de départ` en minutes (float)
	* La `durée du vol` en minutes (float)
	* La `distance` en miles (int)
	* L'`heure d’arrivée prévue` (int)
	* Le `retard à l’arrivée` en minutes (float)
	* Si `le vol a été dévié` ou non (bool)
	* Si `le vol a été annulé` (bool)

Le second concerne les aéroports et les villes correspondantes aux différents codes
IATA (airports.csv) contenant les champs suivants :
	* Le `code IATA` (char)
	* L’`aéroport` (char)
	* La `ville` (char)
	* L’`état` (char)
	* Le `pays` (char)
	* La `latitude` (float)
	* La `longitude` (unsigned float)

Enfin, le dernier fichiers décrit les codes IATA associé à chaque compagnie aérienne (airlines.csv) et contient les champs suivants :
	* `Code IATA` (char)
	* La `compagnie aérienne` (char)

Le but à présent est de réaliser un programme étant capable de charger les fichiers de données et de pouvoir y faire les requêtes suivantes :
* __show-airports__ <airline_id> : affiche tous les aéroports depuis lesquels la compagnie aérienne opère des vols. 
* __show-airlines__ <port_id> : affiche les compagnies aériennes ayant des vols partant de l’aéroport demandé.
* __show-flights__ <port_id> <date>: affiche les vols partant de l’aéroport à la date demandée.
* __most-delayed-flights__ : affiche les 5 vols qui ont subi les plus longs retards à l’arrivée.
* __most-delayed-airlines__ : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards.
* __delayed-airline__ <airline_id> : donne le retard moyen de la compagnie aérienne demandée.
* __changed-flights__ <date> : les vols annulés ou déviés à la date demandée.
* __avg-flight-duration__ <port_id> <port_id> : calcule le temps moyen de vol entre deux aéroports.
* __find-itinerary__ <port_id> <port_id> <date> : affiche un ou plusieurs itinéraires entre deux aéroports à une date donnée.
* __find-multicity-itinerary__ <port_id_depart> <port_id_dest1> <date> … <port_id_destN> <date> : affiche un itinéraire multiville qui permet de visiter plusieurs villes.
* __quit__ : quite.

## COMPILATION ET UTILISATION

la compilation de notre programme est réalisé à l'aide de la commande make permise par le makefile. Le compilateur alors utilisé sera gcc avec toutes les options de compilations habituellement utilisés c'est à dire : "-g -W -Wall -Wextra".
Il suffit alors de simplement taper __make__ afin de compiler

De par cette compilation, un executable sera crée, il faut alors lancer cet executable nommé __bin\projet_PA__ qui devra alors etre lancé à la main. 

## CONTENU

* __source :__ Fichiers sources `.c`
     * `fichier_complet.c` : Ouverture/Fermeture des fichiers .csv +fonctions+ espace d'interactions avec l'utilisateur + libération espace mémoire +...
     * `fichier_complet_(backsave).c` : backsave au cas ou

     * `fonctions.c` : regroupes les fonctions pour le test du makefile
     * `main.c` : interface utilisateur, lecture fichier
* __lib :__ Fichiers librairie `.h`
     * `structures.h` : regroupes les structures
     * `fonctions.h` : regroupes les fonctions
* __bin :__ Fichiers executables
     * `projet_PA`:executable du programme
* __data :__
     * `LICENSE` : Certification de droit d'accès libre aux données.
     * `airlines.csv` : Données des 13 compagnies aériennes.
     * `airports.csv` : Données des 322 aéroports.
     * `flights.csv` : Données des 58492 vols.
* racine:
     * __Makefile :__ Fichier permettant d'effectuer toutes les compilations necessaires.
     * __test_Makefile :__ Fichier permettant d'effectuer toutes les compilations necessaires avec la séparations des .h.
     * __Rapport.pdf :__ Compte-rendu au format PDF expliquand la démarche et la reflexion du projet.
     * __README.md :__ fichier de présentation du projet.
