/* ***************************************************************
			SD pour définir les futurs chargement
******************************************************************/

// --------------------------------------------------------
//struct flights pour table de hachage des listes flights avec pour clef la date

typedef struct Flights {
  int mois;
  int jour;
  int jourSemaine;

  char CompagnieAerienne[MAX_SIZE];
  char AeroportOrigine[MAX_SIZE];
  char AeroportDestination[MAX_SIZE];

  int HeureDepart;
  float DelaiDepart;
  float DureeVol;
  float Distance;
  int HeureArriveePrevue;
  float RetardArrivee;

  int VolDeviee; //si vol dévié --> 1, sinon 0
  int VolAnnule; //si vol annulé --> 1, sinon 0
} Flights ;

typedef struct Cellule_flights{
	Flights vol;
	struct Cellule_flights* next;
}Cellule_flights;

typedef struct Liste_flights{
	Cellule_flights* first;
}Liste_flights;

// --------------------------------------------------------
//struct airlines pour table de hachage des airlines avec pour clef l'aeroport

typedef struct Cellule_Airlines{
	char IATA_CODE[MAX_SIZE];
	char airlines[MAX_SIZE];
	struct Cellule_Airlines * next;
}Cellule_Airlines;

typedef struct Liste_Airlines {
	Cellule_Airlines * first;
}Liste_Airlines;


// --------------------------------------------------------
//struct aeroport pour table de hachage des aeroport avec pour clef la airline
typedef struct Cellule_Aeroport {
	char IATA_CODE[MAX_SIZE];
	char aeroport[MAX_SIZE];
	char ville[MAX_SIZE];
	char etat[MAX_SIZE];
	char pays[MAX_SIZE];
	float latitude;
	float longitude;
	struct Cellule_Aeroport * next;
}Cellule_Aeroport;

typedef struct Liste_Aeroport {
	Cellule_Aeroport * first;
}Liste_Aeroport;

