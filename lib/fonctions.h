/* ***************************************************************
			FONCTION lesture et chargement de données
******************************************************************/

//--------------------------------------
//			Inisialisation

//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI]);
//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI]);

//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX]);


//--------------------------------------
//			table hachage date
//table de hachage avec en clefs la date
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la date correspondante

//fonction qui renvoi la clefs de hachage
int hach_func(int mois, int jour);

//ajout en tete dans la liste correcpondant au code (mois et jour concaténé)
void ajout_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX],int code,int mois,int jour,int jourSemaine,char CompagnieAerienne[MAX_SIZE],
						char AeroportOrigine[MAX_SIZE],char AeroportDestination[MAX_SIZE],int HeureDepart,float DelaiDepart,float DureeVol,
						float Distance,int HeureArriveePrevue,float RetardArrivee,int VolDeviee,int VolAnnule);

//--------------------------------------
//			table hachage airlines
//table de hachage avec en clefs l'aeroport
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la l'aeroport correspondant
//tout en évitant les doublons pour économiser de l'espace mémoire

//fonction qui renvoi la clefs de hachage
int hach_func_airlines(char AeroportOrigine[MAX_SIZE]);

//ajout en tete dans la liste correspondante si celui-ci n'y est pas déjà
void ajout_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],int clef,char CompagnieAerienne[MAX_SIZE]);


//--------------------------------------
//			table hachage aeroport
//table de hachage avec en clefs la airline
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la la airline correspondante
//tout en évitant les doublons pour économiser de l'espace mémoire

//fonction qui renvoi la clefs de hachage
int hach_func_aeroport(char CompagnieAerienne[MAX_SIZE]);

//ajout en tete dans la liste correspondante si celui-ci n'y est pas déjà
void ajout_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI],int clef,char AeroportOrigine[MAX_SIZE]);

//--------------------------------------
//		liste chainne most_delayed
//tliste regtroupant les vols pssedant le plus de retard à l'arrivée
//liste triée donc de par le retard croissant

//on ajout dans la liste chainée le vol selon son 'RetardArrivee'
void ajout_most_delayed(Liste_flights *liste_most_delayed,int mois,int jour,int jourSemaine,char CompagnieAerienne[MAX_SIZE],
						char AeroportOrigine[MAX_SIZE],char AeroportDestination[MAX_SIZE],int HeureDepart,float DelaiDepart,float DureeVol,
						float Distance,int HeureArriveePrevue,float RetardArrivee,int VolDeviee,int VolAnnule);

/* ***************************************************************
					FONCTION lescture/separation
******************************************************************/

//fonction qui va lire et séparer la ligne lu en plusieurs variables
void separation_ligne_flights(Liste_flights table_hachage_date[DATE_MAX],char ligne[TAILLE_LIGNE],Liste_flights *liste_most_delayed,
							Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI]);

/* ***************************************************************
					FONCTION free 
******************************************************************/
//répetitif mais les types envoyées sont différents, nous devons donc creer une fonction free
//pour chaque types (flights,airlines,aeroport)

//fonction qui free une liste_flights de la table de hachage 
void free_liste_hach_date(Liste_flights l);

//fonction qui free une liste_aeroport de la table de hachage 
void free_liste_hach_aeroport(Liste_Aeroport l);
//fonction qui free une liste_airlines de la table de hachage 
void free_liste_hach_airlines(Liste_Airlines l);


//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX]);

//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI]);

//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI]);

//focnctions qui free la liste 'liste_most_delayed'
void free_liste_most_delayed(Liste_flights *l);


/* ***************************************************************
					FONCTION affichages 
******************************************************************/

//on affiche les 5 vols possedant le plus de retard
void afficher_most_delayed(Liste_flights l);

void afficher_flights(Liste_flights l,char port_id[MAX_SIZE]);

void afficher_flights_deviee_annulee(Liste_flights l);

void afficher_airlines(Liste_Airlines l);

void afficher_airports(Liste_Aeroport l);


/* ***************************************************************
					FONCTION requetes 
******************************************************************/

void requete_show_flights(Liste_flights table_hachage_date[DATE_MAX],char port_id[MAX_SIZE],int code);

void requete_changed_flights(Liste_flights table_hachage_date[DATE_MAX],int code);

void requete_show_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],int code);

void requete_show_airports(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI],int code);


