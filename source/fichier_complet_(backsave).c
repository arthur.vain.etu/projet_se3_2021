#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
//#include <liste_chainee.c>

#define NB_AIRLINES 14
#define MAX_SIZE 50
#define DATE_MAX 1231		//équivalent a la date 31/12
#define TAILLE_LIGNE 150
#define LIMITE_MOST_DELAYED 5
#define AEROPORT_MAXI 270		//code ascii de "ZZZ"
#define AIRLINES_MAXI 180		//code ascii de "ZZ"


/* ***************************************************************
			SD pour définir les futurs chargement
******************************************************************/

// --------------------------------------------------------
//struct flights pour table de hachage des listes flights avec pour clef la date

typedef struct Flights {
  int mois;
  int jour;
  int jourSemaine;

  char CompagnieAerienne[MAX_SIZE];
  char AeroportOrigine[MAX_SIZE];
  char AeroportDestination[MAX_SIZE];

  int HeureDepart;
  float DelaiDepart;
  float DureeVol;
  float Distance;
  int HeureArriveePrevue;
  float RetardArrivee;

  int VolDeviee; //si vol dévié --> 1, sinon 0
  int VolAnnule; //si vol annulé --> 1, sinon 0
} Flights ;

typedef struct Cellule_flights{
	Flights vol;
	struct Cellule_flights* next;
}Cellule_flights;

typedef struct Liste_flights{
	Cellule_flights* first;
}Liste_flights;

// --------------------------------------------------------
//struct airlines pour table de hachage des airlines avec pour clef l'aeroport

typedef struct Cellule_Airlines{
	char IATA_CODE[MAX_SIZE];
	char airlines[MAX_SIZE];
	struct Cellule_Airlines * next;
}Cellule_Airlines;

typedef struct Liste_Airlines {
	Cellule_Airlines * first;
}Liste_Airlines;


// --------------------------------------------------------
//struct aeroport pour table de hachage des aeroport avec pour clef la airline
typedef struct Cellule_Aeroport {
	char IATA_CODE[MAX_SIZE];
	char aeroport[MAX_SIZE];
	char ville[MAX_SIZE];
	char etat[MAX_SIZE];
	char pays[MAX_SIZE];
	float latitude;
	float longitude;
	struct Cellule_Aeroport * next;
}Cellule_Aeroport;

typedef struct Liste_Aeroport {
	Cellule_Aeroport * first;
}Liste_Aeroport;


/* ***************************************************************
			FONCTION lesture et chargement de données
******************************************************************/

//--------------------------------------
//			Inisialisation

//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI])
{
	for(int i=0; i<AIRLINES_MAXI;i++)
	{
		table_hachage_aeroport[i].first=NULL;
	}	
}

//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI])
{
	for(int i=0; i<AEROPORT_MAXI;i++)
	{
		table_hachage_airlines[i].first=NULL;
	}	
}

//initialisation de la table de hachage (établi tout les pointeur à NULL)
void init_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX])
{
	for(int i=0; i<DATE_MAX;i++)
	{
		table_hachage_date[i].first=NULL;
	}	
}


//--------------------------------------
//			table hachage date
//table de hachage avec en clefs la date
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la date correspondante

//fonction qui renvoi la clefs de hachage
int hach_func(int mois, int jour)
{
	//établissement du code telle que ABCD avec AB=mois et CD=jour, ex: 0716=> jour 16 et mois 07
	return (mois*100+jour); 
}

//ajout en tete dans la liste correcpondant au code (mois et jour concaténé)
void ajout_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX],int code,int mois,int jour,int jourSemaine,char CompagnieAerienne[MAX_SIZE],
						char AeroportOrigine[MAX_SIZE],char AeroportDestination[MAX_SIZE],int HeureDepart,float DelaiDepart,float DureeVol,
						float Distance,int HeureArriveePrevue,float RetardArrivee,int VolDeviee,int VolAnnule)
{
	Cellule_flights* tmp;
	//initialisation (peut etre le mettre dans une fonction à part)
	tmp=malloc(sizeof(Cellule_flights));
	tmp->vol.mois=mois;
	tmp->vol.jour=jour;
	tmp->vol.jourSemaine=jourSemaine;
	strcpy(tmp->vol.CompagnieAerienne,CompagnieAerienne);
	strcpy(tmp->vol.AeroportOrigine,AeroportOrigine);
	strcpy(tmp->vol.AeroportDestination,AeroportDestination);
	tmp->vol.HeureDepart=HeureDepart;
	tmp->vol.DelaiDepart=DelaiDepart;
	tmp->vol.DureeVol=DureeVol;
	tmp->vol.Distance=Distance;
	tmp->vol.HeureArriveePrevue=HeureArriveePrevue;
	tmp->vol.RetardArrivee=RetardArrivee;
	tmp->vol.VolDeviee=VolDeviee;
	tmp->vol.VolAnnule=VolAnnule;

	//si null on le pose en premier de la liste
	if(table_hachage_date[code].first==NULL)
	{
		table_hachage_date[code].first=tmp;
		return;
	}

	//sinon on ajoute en tete car pas besoin de parcourir c'est pas triée (pourquoi pas trier plus tard)
	tmp->next=table_hachage_date[code].first;
	table_hachage_date[code].first=tmp;
	return;
}

//--------------------------------------
//			table hachage airlines
//table de hachage avec en clefs l'aeroport
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la l'aeroport correspondant
//tout en évitant les doublons pour économiser de l'espace mémoire

//fonction qui renvoi la clefs de hachage
int hach_func_airlines(char AeroportOrigine[MAX_SIZE])

{
	int clef = AeroportOrigine[0] - 'A' + AeroportOrigine[1] - 'A' + AeroportOrigine[2] - 'A';
	return abs(clef);
}

//ajout en tete dans la liste correspondante si celui-ci n'y est pas déjà
void ajout_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],int clef,char CompagnieAerienne[MAX_SIZE])
{
	Cellule_Airlines* tmp;
	//initialisation (peut etre le mettre dans une fonction à part)
	tmp=malloc(sizeof(Cellule_Airlines));
	strcpy(tmp->IATA_CODE,CompagnieAerienne);
	strcpy(tmp->airlines,"");

	//si null on le pose en premier de la liste
	if(table_hachage_airlines[clef].first==NULL)
	{
		table_hachage_airlines[clef].first=tmp;
		return;
	}

	//test si le premier est similaire, si il est similaire on ne fait rien, sinon, on ajout
	if(strcmp(table_hachage_airlines[clef].first->IATA_CODE , tmp->IATA_CODE) == 0)
	{
		free(tmp);
		return;
	}
	if(strcmp(table_hachage_airlines[clef].first->IATA_CODE , tmp->IATA_CODE) != 0)
	{
		tmp->next=table_hachage_airlines[clef].first;
		table_hachage_airlines[clef].first=tmp;
		return;
	}

	//on test si la compagnie aerienne n'est pas déjà écrite dans la liste
	Cellule_Airlines* pt = table_hachage_airlines[clef].first;
	while(pt->next !=NULL)
	{
		if(strcmp(pt->IATA_CODE,CompagnieAerienne)==0)
			{
				free(tmp);
				return;
			} //return si dejà écrit
		pt = pt->next;
	}
	//si le IATA_CODE n'est pas déjà écrit on réalise un ajout tête
	tmp->next=table_hachage_airlines[clef].first;
	table_hachage_airlines[clef].first=tmp;
	return;
}


//--------------------------------------
//			table hachage aeroport
//table de hachage avec en clefs la airline
//les listes chainées ne seront pas triées et seront ajouté en ajout_tete à la la airline correspondante
//tout en évitant les doublons pour économiser de l'espace mémoire

//fonction qui renvoi la clefs de hachage
int hach_func_aeroport(char CompagnieAerienne[MAX_SIZE])
{
	int clef = CompagnieAerienne[0] - 'A' +CompagnieAerienne[1] - 'A';
	return abs(clef);
}

//ajout en tete dans la liste correspondante si celui-ci n'y est pas déjà
void ajout_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI],int clef,char AeroportOrigine[MAX_SIZE])
{
	Cellule_Aeroport* tmp;
	//initialisation (peut etre le mettre dans une fonction à part)
	tmp=malloc(sizeof(Cellule_Aeroport));
	strcpy(tmp->IATA_CODE,AeroportOrigine);
	strcpy(tmp->aeroport,"");
	strcpy(tmp->ville,"");
	strcpy(tmp->etat,"");
	strcpy(tmp->pays,"");
	tmp->latitude = 0;
	tmp->longitude = 0;

	//si null on le pose en premier de la liste
	if(table_hachage_aeroport[clef].first==NULL)
	{
		table_hachage_aeroport[clef].first=tmp;
		return;
	}

	//test si le premier est similaire, si il est similaire on ne fait rien, sinon, on ajout
	if(strcmp(table_hachage_aeroport[clef].first->IATA_CODE , tmp->IATA_CODE) == 0)
	{
		free(tmp);
		return;
	}
	if(strcmp(table_hachage_aeroport[clef].first->IATA_CODE , tmp->IATA_CODE) != 0)
	{
		tmp->next=table_hachage_aeroport[clef].first;
		table_hachage_aeroport[clef].first=tmp;
		return;
	}

	//on test si l'aeroport n'est pas déjà écrite dans la liste
	Cellule_Aeroport* pt = table_hachage_aeroport[clef].first;
	while(pt->next !=NULL)
	{
		if(strcmp(pt->IATA_CODE,AeroportOrigine)==0)
			{
				free(tmp);
				return;
			} //return si dejà écrit
		pt = pt->next;
	}
	//si le IATA_CODE n'est pas déjà écrit on réalise un ajout tête
	tmp->next=table_hachage_aeroport[clef].first;
	table_hachage_aeroport[clef].first=tmp;
	return;}

//--------------------------------------
//		liste chainne most_delayed
//tliste regtroupant les vols pssedant le plus de retard à l'arrivée
//liste triée donc de par le retard croissant

//on ajout dans la liste chainée le vol selon son 'RetardArrivee'
void ajout_most_delayed(Liste_flights *liste_most_delayed,int mois,int jour,int jourSemaine,char CompagnieAerienne[MAX_SIZE],
						char AeroportOrigine[MAX_SIZE],char AeroportDestination[MAX_SIZE],int HeureDepart,float DelaiDepart,float DureeVol,
						float Distance,int HeureArriveePrevue,float RetardArrivee,int VolDeviee,int VolAnnule)
{
	Cellule_flights* tmp;
	//initialisation (peut etre le mettre dans une fonction à part)
	tmp=malloc(sizeof(Cellule_flights));
	tmp->vol.mois=mois;
	tmp->vol.jour=jour;
	tmp->vol.jourSemaine=jourSemaine;
	strcpy(tmp->vol.CompagnieAerienne,CompagnieAerienne);
	strcpy(tmp->vol.AeroportOrigine,AeroportOrigine);
	strcpy(tmp->vol.AeroportDestination,AeroportDestination);
	tmp->vol.HeureDepart=HeureDepart;
	tmp->vol.DelaiDepart=DelaiDepart;
	tmp->vol.DureeVol=DureeVol;
	tmp->vol.Distance=Distance;
	tmp->vol.HeureArriveePrevue=HeureArriveePrevue;
	tmp->vol.RetardArrivee=RetardArrivee;
	tmp->vol.VolDeviee=VolDeviee;
	tmp->vol.VolAnnule=VolAnnule;
	//si null on le pose en premier de la liste
	if(liste_most_delayed->first==NULL)
	{
		liste_most_delayed->first=tmp;
		return;
	}
	//si retard supérieur au premier de la liste
	if((liste_most_delayed->first)->vol.RetardArrivee < tmp->vol.RetardArrivee)
	{
		tmp->next = liste_most_delayed->first;
		liste_most_delayed->first = tmp;
		return;
	}
	//si retard inférieur au premier de la liste et le suivant est NULL
	if((liste_most_delayed->first)->vol.RetardArrivee > tmp->vol.RetardArrivee && liste_most_delayed->first->next == NULL)
	{
		liste_most_delayed->first->next = tmp;
		return;
	}
	//sinon on fait un ajout trié sur le retard (avec taille maxi de la liste de 5)
	Cellule_flights* pt = liste_most_delayed->first->next;
	Cellule_flights* memoire = liste_most_delayed->first;
	int i=0;
	while(pt->next !=NULL)
	{
		if(pt->vol.RetardArrivee < tmp->vol.RetardArrivee)
		{
			memoire->next = tmp;
			tmp->next = pt;
			return;
		}
		memoire = pt;
		pt = pt->next;
		i++; //afin de limiter la taille de la liste
		if(i==LIMITE_MOST_DELAYED)
		{
			free(tmp);
			return;
		}
	}
	pt->next = tmp;
	return;
}

/* ***************************************************************
					FONCTION lescture/separation
******************************************************************/

//fonction qui va lire et séparer la ligne lu en plusieurs variables
void separation_ligne_flights(Liste_flights table_hachage_date[DATE_MAX],char ligne[TAILLE_LIGNE],Liste_flights *liste_most_delayed,
							Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI])
{
	//intisialisation de variables temporaires pour séparer les informations de la ligne
  char mois[MAX_SIZE];
  char jour[MAX_SIZE];
  char jourSemaine[MAX_SIZE];
  char CompagnieAerienne[MAX_SIZE];
  char AeroportOrigine[MAX_SIZE];
  char AeroportDestination[MAX_SIZE];
  char HeureDepart[MAX_SIZE];
  char DelaiDepart[MAX_SIZE];
  char DureeVol[MAX_SIZE];
  char Distance[MAX_SIZE];
  char HeureArriveePrevue[MAX_SIZE];
  char RetardArrivee[MAX_SIZE];
  char VolDeviee[MAX_SIZE];
  char VolAnnule[MAX_SIZE];

	//on sépare la ligne (char*) selon les variables défini ci-dessus
	const char* separator = ","; //défini le séparateur ","
    char *token;
    char *ptr1;
	char *ptr = strdup(ligne); 

    if (ptr == NULL) {
        fprintf(stderr, "strdup failed");
        exit(EXIT_FAILURE);
    }

    ptr1 = ptr;

	//on cherche à récupérer, un a un, tous les mots (token) de la phrase
		//et on commence par le premier(mois)
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(mois,token);
		//on demande le suivant(jour):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(jour,token);
		//on demande le suivant(jourSemaine):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(jourSemaine,token);
		//on demande le suivant(CompagnieAerienne):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(CompagnieAerienne,token);
		//on demande le suivant(AeroportOrigine):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(AeroportOrigine,token);
		//on demande le suivant(AeroportDestination):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(AeroportDestination,token);
		//on demande le suivant(HeureDepart):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(HeureDepart,token);
		//on demande le suivant(DelaiDepart):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(DelaiDepart,token);
		//on demande le suivant(DureeVol):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(DureeVol,token);
		//on demande le suivant(Distance):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(Distance,token);
		//on demande le suivant(HeureArriveePrevue):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(HeureArriveePrevue,token);
		//on demande le suivant(RetardArrivee):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(RetardArrivee,token);
		//on demande le suivant(VolDeviee):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(VolDeviee,token);
		//on demande le suivant(VolAnnule):
    token = strsep(&ptr1, separator);
    if (*token == '\0') {
        token = "0";
    }
   	strcpy(VolAnnule,token);
    free (ptr);
    //////////printf("%s-%d\n",AeroportOrigine,atoi(AeroportOrigine));
    //hach_func => obtenir le code de la fonction en int de format ABCD => AB=mois et CD=jour
	//envoyer les valeurs pour l'ajout dans la table de hachage
	ajout_table_hachage_date(table_hachage_date,hach_func(atoi(mois),atoi(jour)),atoi(mois),atoi(jour),atoi(jourSemaine),
						CompagnieAerienne,AeroportOrigine,AeroportDestination,atoi(HeureDepart),atof(DelaiDepart),
						atof(DureeVol),atof(Distance),atoi(HeureArriveePrevue),atof(RetardArrivee),atoi(VolDeviee),atoi(VolAnnule));
	ajout_most_delayed(liste_most_delayed,atoi(mois),atoi(jour),atoi(jourSemaine),
						CompagnieAerienne,AeroportOrigine,AeroportDestination,atoi(HeureDepart),atof(DelaiDepart),
						atof(DureeVol),atof(Distance),atoi(HeureArriveePrevue),atof(RetardArrivee),atoi(VolDeviee),atoi(VolAnnule));

	int clef_airlines = hach_func_airlines(AeroportOrigine);
	int clef_aeroport = hach_func_aeroport(CompagnieAerienne);
	ajout_table_hachage_airlines(table_hachage_airlines,clef_airlines,CompagnieAerienne);
	ajout_table_hachage_aeroport(table_hachage_aeroport,clef_aeroport,AeroportOrigine);
}

/* ***************************************************************
					FONCTION free 
******************************************************************/
//répetitif mais les types envoyées sont différents, nous devons donc creer une fonction free
//pour chaque types (flights,airlines,aeroport)

//fonction qui free une liste_flights de la table de hachage 
void free_liste_hach_date(Liste_flights l)
{
	if(l.first==NULL)
	{
		return;
	}
	Cellule_flights* tmp=l.first;
	l.first=l.first->next;
	free(tmp);
	free_liste_hach_date(l);
}

//fonction qui free une liste_aeroport de la table de hachage 
void free_liste_hach_aeroport(Liste_Aeroport l)
{
	if(l.first==NULL)
	{
		return;
	}
	Cellule_Aeroport* tmp=l.first;
	l.first=l.first->next;
	free(tmp);
	free_liste_hach_aeroport(l);
}

//fonction qui free une liste_airlines de la table de hachage 
void free_liste_hach_airlines(Liste_Airlines l)
{
	if(l.first==NULL)
	{
		return;
	}
	Cellule_Airlines* tmp=l.first;
	l.first=l.first->next;
	free(tmp);
	free_liste_hach_airlines(l);
}


//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_date(Liste_flights table_hachage_date[DATE_MAX])
{
	for(int i=0; i<DATE_MAX;i++)
	{
		free_liste_hach_date(table_hachage_date[i]);
	}
}

//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI])
{
	for(int i=0; i<AEROPORT_MAXI;i++)
	{
		free_liste_hach_airlines(table_hachage_airlines[i]);
	}
}

//fonction qui parcours l'ensemble de la table de hachage afin de free les listes unes par une
void free_table_hachage_aeroport(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI])
{
	for(int i=0; i<AIRLINES_MAXI;i++)
	{
		free_liste_hach_aeroport(table_hachage_aeroport[i]);
	}
}

//focnctions qui free la liste 'liste_most_delayed'
void free_liste_most_delayed(Liste_flights *l)
{
	if(l->first==NULL)
	{
		return;
	}
	Cellule_flights* tmp = l->first;
	l->first = l->first->next;
	free(tmp);
	free_liste_most_delayed(l);
}


/* ***************************************************************
					FONCTION affichages 
******************************************************************/

//on affiche les 5 vols possedant le plus de retard
void afficher_most_delayed(Liste_flights l)
{
	if(l.first==NULL)
	{
		printf("erreur pas possible mdr\n");
	}
	int i=0; //pour compter et print que les 5 plus en retard
	while(l.first != NULL && i<LIMITE_MOST_DELAYED)
		{
			printf("%d,%d,%d,%s,%s,%s,%d,%f,%f,%f,%d,%f,%d,%d\n",(l.first->vol).mois,l.first->vol.jour,l.first->vol.jourSemaine,l.first->vol.CompagnieAerienne,l.first->vol.AeroportOrigine,l.first->vol.AeroportDestination,
						(l.first->vol).HeureDepart,(l.first->vol).DelaiDepart,(l.first->vol).DureeVol,l.first->vol.Distance,l.first->vol.HeureArriveePrevue,
						(l.first->vol).RetardArrivee,(l.first->vol).VolDeviee,(l.first->vol).VolAnnule);
			l.first=l.first->next;
			i++;
		}
	return;
}

void afficher_flights(Liste_flights l,char port_id[MAX_SIZE])
{
	if(l.first==NULL)
	{
		printf("aucuns flights ne partent de cet aeroport à la date selectionne...");
	}

	while(l.first != NULL){
		if(strcmp(port_id,(l.first->vol).AeroportOrigine) == 0)
		{

			printf("%d,%d,%d,%s,%s,%s,%d,%f,%f,%f,%d,%f,%d,%d\n",(l.first->vol).mois,l.first->vol.jour,l.first->vol.jourSemaine,l.first->vol.CompagnieAerienne,l.first->vol.AeroportOrigine,l.first->vol.AeroportDestination,
							(l.first->vol).HeureDepart,(l.first->vol).DelaiDepart,(l.first->vol).DureeVol,l.first->vol.Distance,l.first->vol.HeureArriveePrevue,
							(l.first->vol).RetardArrivee,(l.first->vol).VolDeviee,(l.first->vol).VolAnnule);
		}
		l.first=l.first->next;
	}

}

void afficher_flights_deviee_annulee(Liste_flights l)
{
	if(l.first==NULL)
	{
		printf("aucuns flights ne partent de cet aeroport à la date selectionne...");
	}
	while(l.first != NULL){
		if((l.first->vol.VolDeviee)==1 || (l.first->vol.VolAnnule)==1)
		{

			printf("%d,%d,%d,%s,%s,%s,%d,%f,%f,%f,%d,%f,%d,%d\n",(l.first->vol).mois,l.first->vol.jour,l.first->vol.jourSemaine,l.first->vol.CompagnieAerienne,l.first->vol.AeroportOrigine,l.first->vol.AeroportDestination,
							(l.first->vol).HeureDepart,(l.first->vol).DelaiDepart,(l.first->vol).DureeVol,l.first->vol.Distance,l.first->vol.HeureArriveePrevue,
							(l.first->vol).RetardArrivee,(l.first->vol).VolDeviee,(l.first->vol).VolAnnule);
		}
		l.first=l.first->next;
	}
}

void afficher_airlines(Liste_Airlines l)
{
	if(l.first==NULL)
	{
		printf("Aucunes compagnie aérienne ne partent de cet aeroport...\nEtre vous sur d'avoir bien écrit le iata_code ?");
	}

	while(l.first != NULL){
			printf("%s,%s\n",l.first->IATA_CODE,l.first->airlines);
		l.first=l.first->next;
	}

}

void afficher_airports(Liste_Aeroport l)
{
	if(l.first==NULL)
	{
		printf("Aucuns aéroport desquels la compagnie cité n'opère de vols....\nEtre vous sur d'avoir bien écrit le iata_code ?");
	}

	while(l.first != NULL){
			printf("%s,%s,%s,%s,%s,%f,%f\n",l.first->IATA_CODE,l.first->aeroport,l.first->ville,l.first->etat,l.first->pays,l.first->latitude,l.first->longitude);
		l.first=l.first->next;
	}

}


/* ***************************************************************
					FONCTION requetes 
******************************************************************/

void requete_show_flights(Liste_flights table_hachage_date[DATE_MAX],char port_id[MAX_SIZE],int code)
{
	afficher_flights(table_hachage_date[code],port_id);
	return;
}

void requete_changed_flights(Liste_flights table_hachage_date[DATE_MAX],int code)
{
	afficher_flights_deviee_annulee(table_hachage_date[code]);
	return;
}

void requete_show_airlines(Liste_Airlines table_hachage_airlines[AEROPORT_MAXI],int code)
{
	afficher_airlines(table_hachage_airlines[code]);
	return;
}
void requete_show_airports(Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI],int code)
{
	afficher_airports(table_hachage_aeroport[code]);
	return;
}




/* ***************************************************************
					FONCTION MAIN 
******************************************************************/




int main(){

	//creation liste chaine most delayed
	Liste_flights liste_most_delayed;
	liste_most_delayed.first=NULL;

	//création tableaut de la table de hachage de structure 'Aeroport' triée par Aeroport
	Liste_Airlines table_hachage_airlines[AEROPORT_MAXI];
	//création tableaut de la table de hachage de structure 'Airlines' triée par Airlines
	Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI];
	//création tableau de la table de hachage de structure 'Flights' triée par date
	Liste_flights table_hachage_date[DATE_MAX];
	//init tables de hachages
	init_table_hachage_date(table_hachage_date);
	init_table_hachage_aeroport(table_hachage_aeroport);
	init_table_hachage_airlines(table_hachage_airlines);

/* -----------------------------------------------------------------
			Ouverture fichier et chargement des donnes
------------------------------------------------------------------*/
	

	//ouverture du fichier airline.csv
	FILE* fd_airlines=NULL;
	fd_airlines=fopen("data/airlines.csv","r");  //"r+" parce que si on met que "r" on peut seulement lire et pas écrire
	if(fd_airlines != NULL){
		printf("L'ouverture du fichier airlines.csv est un succès ! \n");
		fclose(fd_airlines);
		printf("Fichier airlines.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier airlines.csv... \n");
	}

	//ouverture du fichier airports.csv
	FILE* fd_airports;
	fd_airports=fopen("data/airports.csv","r");
	if(fd_airports != NULL){
		//on peut lire et écrire dans le fichier flights.csv

		printf("L'ouverture du fichier airports.csv est un succés ! \n");
		fclose(fd_airports);
		printf("Fichier airports.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier airports.csv... \n");
	}

	//ouverture du fichier flights.csv
	//test table de hachage
	FILE* fd_flights;
	fd_flights=fopen("data/flights.csv","r");
	printf("fichier bien ouvert\n");

	if(fd_flights != NULL){
		//initialisation de la variable contenant la ligne lu:			
		char ligne[TAILLE_LIGNE] = ""; //chaine vide de taille TAILLE_LIGNE
		fgets(ligne,TAILLE_LIGNE,fd_flights);//lecture de la première ligne de texte sans données utiles
		while(!feof(fd_flights))
		{
			fgets(ligne,TAILLE_LIGNE,fd_flights);//lecture des autres lignes de données
			////printf("%s\n",ligne); pour tester si ça marche
			separation_ligne_flights(table_hachage_date,ligne,&liste_most_delayed,table_hachage_airlines,table_hachage_aeroport);
		}

		printf("L'ouverture du fichier flights.csv est un succes ! \n");
		fclose(fd_flights);
		printf("Fichier flights.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier flights.csv... \n");
	}


	printf("\n\n\n\n");


/* -----------------------------------------------------------------
					Espace utilisateur
------------------------------------------------------------------*/

	
	int choix_utilisateur='h';
	do{  //boucle faire tant que pour permettre de réaliser plusieurs types de selections et donc de cumuler ces outils.
		printf("entrez le code correspondant à l'outil voulu, entrez:\n"
				"1 - pour la requete show_flights;\n"
				"2 - pour la requete changed_flights;\n"
				"3 - pour la requete most_delayed_flights\n"
				"4 - pour la requete show_airlines\n"
				"5 - pour la requete show_airport\n"
				"9 - pour quitter et désallouer la mémoire\n");
		scanf("%d",&choix_utilisateur);
   		printf("vous avez selectionne : %d\n",choix_utilisateur);

		switch(choix_utilisateur) //une fonction switch permettant de choisir l'outils à selectionner
   		{
	   		case 1://requete show_flights   
   			{
   				printf("requete selectionée: show_flights <port_id> <date>,\nveuillez indiquer <port_id> en majuscules et <date> au format jour/mois:");
				char port_id[MAX_SIZE]="";
				int mois=0,jour=0;
				scanf("%s %d/%d",port_id,&jour,&mois);
				printf("voici les vols partant de %s à la date:%d/%d \n\n",port_id,jour,mois);
				requete_show_flights(table_hachage_date,port_id,hach_func(mois,jour));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}
  	 
   			case 2://requete changed_flights
   			{
   				printf("requete selectionée: changed_flights <date>,\nveuillez indiquer <date> au format jour/mois:");
   				int mois=0,jour=0;
				scanf("%d/%d",&jour,&mois);
				printf("voici les vols déviés ou annulés à la date:%d/%d \n\n",jour,mois);
				requete_changed_flights(table_hachage_date,hach_func(mois,jour));
	   			printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

   			case 3://requete most_delayed_flights
	   		{
	   			printf("requete selectionée: most_delayed_flights,\n");
				printf("voici les vols les plus retardés\n\n");
				afficher_most_delayed(liste_most_delayed);
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

	   		case 4://requete show_airlines   
   			{
   				printf("requete selectionée: show_airlines <port_id>,\nveuillez indiquer <port_id> en majuscules:");
				char port_id[MAX_SIZE]="";
				scanf("%s",port_id);
				printf("voici les compagnies opérant de %s\n\n",port_id);
				requete_show_airlines(table_hachage_airlines,hach_func_airlines(port_id));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

	   		case 5://requete show_airports  
   			{
   				printf("requete selectionée: show_airports <airline_id>,\nveuillez indiquer <airline_id> en majuscules:");
				char airline_id[MAX_SIZE]="";
				scanf("%s",airline_id);
				printf("voici les aeroports ou interviens la compagnie %s\n\n",airline_id);
				requete_show_airports(table_hachage_aeroport,hach_func_aeroport(airline_id));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

   			default://message erreur si l'entrée n'est pas une de celle attendue
     		{
     			if(choix_utilisateur != 9)
     			{
					printf("Ce n'est pas '1' ou '2' ou '3' ou '4' ou '5' !\n");
		    	}
		    }
		}
	}while(choix_utilisateur != 9);

	printf("\nmerci d'avoir utilisé notre programme, à bientot !\n");

/* -----------------------------------------------------------------
					Desallocation memoire
------------------------------------------------------------------*/

	printf("liste chainée airlines désalloc\n");
	free_liste_most_delayed(&liste_most_delayed);
	printf("liste chainée most delayed désalloc\n");
	free_table_hachage_airlines(table_hachage_airlines);
	printf("table de hachage triée par airlines désalloc\n");
	free_table_hachage_aeroport(table_hachage_aeroport);
	printf("table de hachage triée par aeroports désalloc\n");
	free_table_hachage_date(table_hachage_date);
	printf("table de hachage triée par date désalloc\ndésallocation mémoire terminée !\n");
	return 0;
}
