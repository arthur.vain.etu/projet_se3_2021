#include "fonctions.h"



/* ***************************************************************
					FONCTION MAIN
******************************************************************/




int main(){

	//creation liste chaine most delayed
	Liste_flights liste_most_delayed;
	liste_most_delayed.first=NULL;

	//création tableaut de la table de hachage de structure 'Aeroport' triée par Aeroport
	Liste_Airlines table_hachage_airlines[AEROPORT_MAXI];
	//création tableaut de la table de hachage de structure 'Airlines' triée par Airlines
	Liste_Aeroport table_hachage_aeroport[AIRLINES_MAXI];
	//création tableau de la table de hachage de structure 'Flights' triée par date
	Liste_flights table_hachage_date[DATE_MAX];
	//init tables de hachages
	init_table_hachage_date(table_hachage_date);
	init_table_hachage_aeroport(table_hachage_aeroport);
	init_table_hachage_airlines(table_hachage_airlines);

/* -----------------------------------------------------------------
			Ouverture fichier et chargement des donnes
------------------------------------------------------------------*/


	//ouverture du fichier airline.csv
	FILE* fd_airlines=NULL;
	fd_airlines=fopen("data/airlines.csv","r");  //"r+" parce que si on met que "r" on peut seulement lire et pas écrire
	if(fd_airlines != NULL){
		printf("L'ouverture du fichier airlines.csv est un succès ! \n");
		fclose(fd_airlines);
		printf("Fichier airlines.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier airlines.csv... \n");
	}

	//ouverture du fichier airports.csv
	FILE* fd_airports;
	fd_airports=fopen("data/airports.csv","r");
	if(fd_airports != NULL){
		//on peut lire et écrire dans le fichier flights.csv

		printf("L'ouverture du fichier airports.csv est un succés ! \n");
		fclose(fd_airports);
		printf("Fichier airports.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier airports.csv... \n");
	}

	//ouverture du fichier flights.csv
	//test table de hachage
	FILE* fd_flights;
	fd_flights=fopen("data/flights.csv","r");
	printf("fichier bien ouvert\n");

	if(fd_flights != NULL){
		//initialisation de la variable contenant la ligne lu:
		char ligne[TAILLE_LIGNE] = ""; //chaine vide de taille TAILLE_LIGNE
		fgets(ligne,TAILLE_LIGNE,fd_flights);//lecture de la première ligne de texte sans données utiles
		while(!feof(fd_flights))
		{
			fgets(ligne,TAILLE_LIGNE,fd_flights);//lecture des autres lignes de données
			////printf("%s\n",ligne); pour tester si ça marche
			separation_ligne_flights(table_hachage_date,ligne,&liste_most_delayed,table_hachage_airlines,table_hachage_aeroport);
		}

		printf("L'ouverture du fichier flights.csv est un succes ! \n");
		fclose(fd_flights);
		printf("Fichier flights.csv bien fermé !\n");
	}
	else{
		printf("Impossible d'ouvrir le fichier flights.csv... \n");
	}


	printf("\n\n\n\n");


/* -----------------------------------------------------------------
					Espace utilisateur
------------------------------------------------------------------*/


	int choix_utilisateur='h';
	do{  //boucle faire tant que pour permettre de réaliser plusieurs types de selections et donc de cumuler ces outils.
		printf("entrez le code correspondant à l'outil voulu, entrez:\n"
				"1 - pour la requete show_flights;\n"
				"2 - pour la requete changed_flights;\n"
				"3 - pour la requete most_delayed_flights\n"
				"4 - pour la requete show_airlines\n"
				"5 - pour la requete show_airport\n"
				"9 - pour quitter et désallouer la mémoire\n");
		scanf("%d",&choix_utilisateur);
   		printf("vous avez selectionne : %d\n",choix_utilisateur);

		switch(choix_utilisateur) //une fonction switch permettant de choisir l'outils à selectionner
   		{
	   		case 1://requete show_flights
   			{
   				printf("requete selectionée: show_flights <port_id> <date>,\nveuillez indiquer <port_id> en majuscules et <date> au format jour/mois:");
				char port_id[MAX_SIZE]="";
				int mois=0,jour=0;
				scanf("%s %d/%d",port_id,&jour,&mois);
				printf("voici les vols partant de %s à la date:%d/%d \n\n",port_id,jour,mois);
				requete_show_flights(table_hachage_date,port_id,hach_func(mois,jour));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

   			case 2://requete changed_flights
   			{
   				printf("requete selectionée: changed_flights <date>,\nveuillez indiquer <date> au format jour/mois:");
   				int mois=0,jour=0;
				scanf("%d/%d",&jour,&mois);
				printf("voici les vols déviés ou annulés à la date:%d/%d \n\n",jour,mois);
				requete_changed_flights(table_hachage_date,hach_func(mois,jour));
	   			printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

   			case 3://requete most_delayed_flights
	   		{
	   			printf("requete selectionée: most_delayed_flights,\n");
				printf("voici les vols les plus retardés\n\n");
				afficher_most_delayed(liste_most_delayed);
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

	   		case 4://requete show_airlines
   			{
   				printf("requete selectionée: show_airlines <port_id>,\nveuillez indiquer <port_id> en majuscules:");
				char port_id[MAX_SIZE]="";
				scanf("%s",port_id);
				printf("voici les compagnies opérant de %s\n\n",port_id);
				requete_show_airlines(table_hachage_airlines,hach_func_airlines(port_id));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

	   		case 5://requete show_airports
   			{
   				printf("requete selectionée: show_airports <airline_id>,\nveuillez indiquer <airline_id> en majuscules:");
				char airline_id[MAX_SIZE]="";
				scanf("%s",airline_id);
				printf("voici les aeroports ou interviens la compagnie %s\n\n",airline_id);
				requete_show_airports(table_hachage_aeroport,hach_func_aeroport(airline_id));
   				printf("\n\nVoulez-vous effectuer une autre selection ?\nSi vous voulez vous arrêter là, taper '9'\n");
   				break;
   			}

   			default://message erreur si l'entrée n'est pas une de celle attendue
     		{
     			if(choix_utilisateur != 9)
     			{
					printf("Ce n'est pas '1' ou '2' ou '3' ou '4' ou '5' !\n");
		    	}
		    }
		}
	}while(choix_utilisateur != 9);

	printf("\nmerci d'avoir utilisé notre programme, à bientot !\n");

/* -----------------------------------------------------------------
					Desallocation memoire
------------------------------------------------------------------*/

	printf("liste chainée airlines désalloc\n");
	free_liste_most_delayed(&liste_most_delayed);
	printf("liste chainée most delayed désalloc\n");
	free_table_hachage_airlines(table_hachage_airlines);
	printf("table de hachage triée par airlines désalloc\n");
	free_table_hachage_aeroport(table_hachage_aeroport);
	printf("table de hachage triée par aeroports désalloc\n");
	free_table_hachage_date(table_hachage_date);
	printf("table de hachage triée par date désalloc\ndésallocation mémoire terminée !\n");
	return 0;
}
